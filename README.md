# Online Key Value Database and Data Relay

## 

```console
put</get/push/pop/del/qdel>.meeiot.org/user/<key>=<value> <HTTP_POST <hdr='headers,...'> <multipart> <json> >
```

    PUT / GET to key value storage
    PUSH / POP  to queue value storage
    

### API's
    put - stores a value under a key
    get - get's a value from a key, updates last access time to ttl
    push - push a value to the head of the key queue. (keep adding)
    pop - pops (removes) a value from the tail of the key queue. updates last access time for ttl
    del - deletes the key / or a key      
        https::del.meeiot.org/TOKEN         all the keys and values for token
        https::del.meeiot.org/TOKEN/key     just the key and it's val
    qdel - deletes all the queue(s) or one queue
        https::qdel.meeiot.org/TOKEN        all the queues
        https::qdel.meeiot.org/TOKEN/que    just the queue
        
    ttl 24 hours for keys and queues since last put/push
    access log 4 days ttl

    https://put.meeiot.org/<userid>/<key><=value>  POST (multipart-data)(json)
    
### SAMPLES
    Test UID: 5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f
        Operations using demo user id has a delay of 8 secoonds.
    see the meeIOTtest.sh file
        see the extra hdr="headers,..."  option
        If the POST has a multipart field hdr="HTTP_HEADERS,COMMA,SEPARATED" when the 
        data is being pulled back, the headers are send. This allows to store a media / mime
        type and get it back right into the browser without any post processing.
   
#### All samples are in curl so they can be easy ported to any language




```console
#!/bin/bash
token="5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f"
delay=4
echo "Test with free user token. Delay between put's/gets $delay seconds. Adding some values"
curl -k  http://put.meeiot.org/${token}/value=xxxxxxxx
sleep $delay
curl  -k http://put.meeiot.org/${token}/value1=somevalue
sleep $delay
curl  -k http://put.meeiot.org/${token}/value2=zzzzzzzzzzzzz
sleep $delay
echo "getting all keys"
sleep $delay

curl -k http://get.meeiot.org/${token}
echo ""
echo "getting all values"
sleep $delay
curl -k http://get.meeiot.org/${token}/value
echo ""
sleep $delay
curl -k http://get.meeiot.org/${token}/value1
echo ""
sleep $delay
curl -k http://get.meeiot.org/${token}/value2
echo " deleting all keys"
sleep $delay
curl -k http://del.meeiot.org/${token}
echo "getting all keys"
sleep $delay
curl -k http://get.meeiot.org/${token}

sleep $delay

echo "sending a text file as multipart"
curl  -k -F file1=@testfile http://put.meeiot.org/${token}
sleep $delay
echo "getting all keys"
echo ""
sleep $delay
curl -k http://get.meeiot.org/${token}
echo ""
echo "getting file"
sleep $delay
curl  -k http://get.meeiot.org/${token}/file1
echo ""

echo "put an image as raw then with headers"
sleep $delay
curl  -F imageweb=@./meeiot.jpg -F hdr="Content-Type: image/jpeg"  http://put.meeiot.org/${token}
sleep $delay
curl   -F image=@./meeiot.jpg http://put.meeiot.org/${token}
echo "get  image as raw"
echo "open the link in the browser:. See headers for content type are OK"
echo " http://get.meeiot.org/${token}/image"
read dummy
echo "get  image as web with http headers. open thislink in browser. then press a key to continue"
sleep $delay
echo  http://get.meeiot.org/${token}/imageweb
read dummy

echo "push an image as raw then with headers"
sleep $delay
curl  -F imageweb=@./meeiot.jpg -F hdr="Content-Type: image/jpeg"  http://push.meeiot.org/${token}
sleep $delay
curl   -F image=@./meeiot.jpg http://push.meeiot.org/${token}
echo "get  image as raw. open link in browser"
echo "http://pop.meeiot.org/${token}/image"
read dummy

echo "get  image as web with http headers. open thislink in browser. then press a key to continue"
sleep $delay
echo  http://pop.meeiot.org/${token}/imageweb
read dummy

echo "put a jsonraw to [jsonraw] = {...} and get it back raw "
sleep $delay
curl -H "Content-Type: application/json" -X POST -d '{"value":"xyz"}' http://put.meeiot.org/${token}/jsonraw
echo "keys for user"
sleep $delay
curl  http://get.meeiot.org/${token}
echo "getting back json"
curl  http://get.meeiot.org/${token}/jsonraw

echo "put a json and get it back with HTTP headers application/json "
sleep $delay
curl  -F jsonweb=@./json.txt -F hdr="Content-Type: application/json"  http://put.meeiot.org/${token}
echo "open the link in the browser:. See headers for content type are OK"
echo https://get.meeiot.org/${token}/jsonweb
read dummy;
```


```console
# GET variation 
# unsecure
curl  http://put.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/mykey=whatever # stores whatever
curl  http://get.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/whatever   # get's whatever
curl  http://get.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f            # gets all keys, mykey,


curl  http://push.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myq=1  # puts 1 in q
# secure
curl -k https://push.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myq=1  # puts 1 in q
curl -k https://push.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myq=3  # puts 3 in q
curl -k https://pop.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myq     # returns 1
curl -k https://pop.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myq     # returns 1
curl -k https://pop.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myq     # returns 3

#REST with HTTP_POST
curl -k -H "Content-Type: application/json" -X POST -d '{"value":"xyz"}' https://put.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myjson

curl -k -v -H "Content-Type: application/json" -X POST -d @./json.txt https://put.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/myjson

# FILE POSTING is forbidden with this demo ID
curl -k -F mykey=@testfile https://put.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f
```
        
#### Error Codes

```console
0:X     success:scheme used
1:*     user id missing
2:*     value missing
3:*     
4:*     empty json post
5:*     key  too big or to small
6:*     value too big for the account, or empty
7:*     json too big for the account, or empty
8:*     multipart attachment FILE to big for this type of account
9:*     user id mismatch
10:*    user key mismatch. Error++, on 3 errors IP is banned.
11:*    access time to early since last access
12:*    access time error. could not determine last access time.
13:*    delete operation not allowed from other then put/push ip (demo id only)
14:*    client ip retrieval fail
20:*    system error
30:*    database err
31:*    database err
60:*    database err
61:*    database err
99:*    invalid request
111:*   no more data for the key

```

### PHP

```javascript
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://put.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/mydata");
curl_setopt($ch, CURLOPT_POST, 1);
$payload = json_encode( array( "jsonkey"=> "json value" ) );
curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
$result = curl_exec($ch);
curl_close($ch);
echo "$result";

// or just inline using GET

file_get_contents("https://put.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/mydata={$MYDATA}");
file_get_contents("https://get.meeiot.org/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/mydata");

````

### ARDUINO
Using https://www.arduino.cc/en/Tutorial/WebClient

```javascript
//
// share data
//
EthernetClient client;
if (client.connect(server, 80)) 
{
    char req[200];
    sprintf(req,""GET /put/5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f/mysensor=$d HTTP/1.1", sensorvalue);
    client.println(req);
    client.println("Host: put.meeiot.org");
    client.println("Connection: close");
    client.println();
    while (!client.available()) {delay(1000);}
    char c = client.read();
    Serial.print(c);
    client.close();    
  }
}
```

### LINUX CBASH SCRIPT, running form cron. 


```console
p=`ping -c 1 meeiot.org 2>&1 | grep "0% packet loss"`
[[ -z ${p} ]] && exit
JSON_TO_SHARE+/tmp/appjson
[[ ! -f ${JSON_TO_SHARE} ]] && exit
MYID="5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f"
curl -vX POST https://put.meeiot.org/${NYID}/mydata -d @${JSON_TO_SHARE}  --header "Content-Type: application/json"
```

### LINUX CRON script sharing live image motion camera into meeIOT queue. 
```
#/bin/bash
#
# cron script with liveimage timelapse or motion detection image sharing trough meeiot.
#
p=`ping -c 1 meeiot.org 2>&1 | grep "0% packet loss"`
[[ -z ${p} ]] && exit
TOKEN="5ba8e5788973226b3255844b37221c2a0f8a3921784b8cf1c93b9f" ## need a paid account to allow over 16k POST's
pushd /data/snaps
image=$(ls -rt | tail --lines=1)
if [[ ! -z $lastfile ]];then
        curl -k -F img=@$image https://push.meeiot.org/5aafe8c0cebc1ca6b729a84b2d3f364925942e720e62d9509f87fd
fi
popd
```

### Relaying nano-pi motion detection camera running liveimage + above script(see projects)
    
https://meeiot.org/liveimage.php     (under  the table)



